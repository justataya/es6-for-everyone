export function fight(firstFighter, secondFighter) {
  while (true) {
    if (firstFighter.health > 0) {
      attackEnemy(firstFighter, secondFighter);
    } else {
      return secondFighter;
    }

    if (secondFighter.health > 0) {
      attackEnemy(secondFighter, firstFighter);
    } else {
      return firstFighter;
    }
  }
}

function attackEnemy(attacker, enemy) {
  enemy.health -= getDamage(attacker, enemy);
}

export function getDamage(attacker, enemy) {
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  return Math.max(0, damage);
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
