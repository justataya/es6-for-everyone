import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const title = '	🎉 Winner!	🎉';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}


function createWinnerDetails(fighter) {
  const { name, source } = fighter;

  const winnerDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const imageElement = createElement({ tagName: 'img', className: 'image-attack' });


  nameElement.innerText = `Name: ${name}`;
  imageElement.src = source;

  winnerDetails.append(nameElement);
  winnerDetails.append(imageElement);

  return winnerDetails;
}